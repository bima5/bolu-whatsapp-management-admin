# HaiBolu Admin Dashboard

Aplikasi Admin Dashboard yang digunakan untuk melakukan manajemen dan monitoring data dan rules yang terdapat pada HaiBolu Web App.

## Project Setup

- Clone repositori ini
- Jalankan npm install
- Buat file .env baru dari .env.example
- Setup file .env
- Jalankan npm run serve

## Technology Stacks

Aplikasi ini dibangun dengan menggunakan beberapa dependency, framework, library, dan package di bawah ini :

| Nama                     | Versi        |
| ------------------------ | ------------ |
| Vue                      | 2.6.12       |
| Vuex                     | 3.5.1        |
| Vuelidate                | 0.7.6        |
| Vuedraggable             | 2.24.3       |
| babel-eslint             | 10.1.0       |
| Eslint                   | 6.7.2        |
| Sass                     | 1.26.3       |
| Sass Loader              | 8.0.2        |
| ApexCharts.js            | 3.22.2       |
| Axios                    | 0.21.1       |
| Bootstrap                | 4.5.3        |
| Bootstrap Vue            | 2.19.0       |
| Chart.js                 | 2.9.4        |
| CKEditor 4 Vue           | 1.3.0        |
| Core-js                  | 3.7.0        |
| Crypto-js                | 4.0.0        |
| ECharts                  | 4.9.0        |
| Fabric                   | 4.5.0        |
| Firebase                 | 8.1.1        |
| JWT-Decode               | 3.1.2        |
| Leaflet                  | 1.7.1        |
| Slugify                  | 1.6.0        |
| SweetAlert2              | 10.10.1      |
| metismenujs              | 1.2.1        |
| qrcode.vue               | 1.3.3        |
| @vue/cli-plugin-babel    | 4.3.0        |
| @vue/cli-plugin-eslint   | 4.3.0        |
| @vue/cli-service         | 4.3.0        |
| eslint-plugin-vue        | 6.2.2        |
| register-service-worker  | 1.7.2        |
| simplebar-vue            | 1.6.0        |
| v-click-outside          | 3.1.2        |
| v-img                    | 0.2.0        |
| v-mask                   | 2.2.3        |
| v-select2-component      | 0.4.7        |
| vue-apexcharts           | 1.6.0        |
| vue-chartist             | 2.3.1        |
| vue-chartjs              | 3.5.1        |
| vue-clipboard2           | 0.3.1        |
| vue-easy-lightbox        | 0.13.0       |
| vue-echarts              | 5.0.0-beta.0 |
| vue-form-wizard          | 0.8.4        |
| vue-i18n                 | 8.22.2       |
| vue-meta                 | 2.4.0        |
| vue-moment               | 4.1.0        |
| vue-multiselect          | 2.1.6        |
| vue-number-input-spinner | 2.2.0        |
| vue-qrcode               | 1.0.0        |
| vue-quill-editor         | 3.0.6        |
| vue-rangedate-picker     | 1.0.0        |
| vue-router               | 3.4.9        |
| vue-search-select        | 2.9.3        |
| vue-slide-bar            | 1.2.0        |
| vue-star-rating          | 1.7.0        |
| vue-sweetalert2          | 4.3.1        |
| vue-switches             | 2.0.1        |
| vue-youtube              | 1.4.0        |
| vue-template-compiler    | 2.6.11       |
| vue2-datepicker          | 3.8.0        |
| vue2-dropzone            | 3.6.0        |
| vue2-google-maps         | 0.10.7       |
| vue2-leaflet             | 2.6.0        |

## Maintainer

- [Bima Pangestu](https://gitlab.com/BimaPangestu28)
- [Afwa Bagas Wahuda](https://gitlab.com/wahudamon)

<!-- ## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn run serve
```

### Compiles and minifies for production

```
yarn run build
```

### Run your tests

```
yarn run test
```

### Lints and fixes files

```
yarn run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/). -->
