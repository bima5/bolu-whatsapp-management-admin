export const state = {
  type: null,
  message: null,
};

export const getters = {};

export const mutations = {
  success(state, message) {
    state.type = "success";
    state.message = message;
  },
  error(state, message) {
    state.type = "danger";
    state.message = message;
  },
  info(state, message) {
    state.type = "info";
    state.message = message;
  },
  clear(state) {
    state.type = null;
    state.message = null;
  },
};

export const actions = {
  success({ commit }, message) {
    commit("success", message);
  },
  error({ commit }, message) {
    commit("error", message);
  },
  info({ commit }, message) {
    commit("info", message);
  },
  clear({ commit }) {
    commit("clear");
  },
};
