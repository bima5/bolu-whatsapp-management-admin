import whatsappSendMessageApi from "../../api/whatsapp-send-message.api";
import filterDefault from "../../helpers/filter";

export const state = {
  whatsappSendMessages: [],
  whatsappSendMessage: {},
  total: 0,
  filter: {
    ...filterDefault,
  },
};

export const getters = {
  whatsappSendMessages(state) {
    return state.whatsappSendMessages;
  },

  filter(state) {
    return state.filter;
  },

  total(state) {
    return state.total;
  },
};

export const mutations = {
  SET_WHATSAPP_NUMBER_HOSTS(state, whatsappSendMessages) {
    state.whatsappSendMessages = whatsappSendMessages;
  },

  ADD_WHATSAPP_NUMBER_HOST(state, whatsappSendMessage) {
    state.whatsappSendMessages.unshift(whatsappSendMessage);
  },

  MERGE_WHATSAPP_NUMBER_HOSTS(state, whatsappSendMessages) {
    state.whatsappSendMessages = whatsappSendMessages.concat(
      state.whatsappSendMessages
    );
  },

  SET_FILTER(state, { key, value }) {
    state.filter[key] = value;
  },

  REMOVE_WHATSAPP_NUMBER_HOST(state, id) {
    state.whatsappSendMessages.splice(
      state.whatsappSendMessages.findIndex(
        (whatsappSendMessage) => whatsappSendMessage.id === id
      ),
      1
    );
  },

  SET_WHATSAPP_NUMBER_HOST(state, whatsappSendMessage) {
    state.whatsappSendMessage = whatsappSendMessage;
  },

  CHANGE_WHATSAPP_NUMBER_HOST(state, { id, whatsappSendMessage }) {
    state.whatsappSendMessages[
      state.whatsappSendMessages.findIndex(
        (whatsappSendMessage) => whatsappSendMessage.id === id
      )
    ] = whatsappSendMessage;
  },

  SET_TOTAL(state, total) {
    state.total = total;
  },

  SET_STATUS(state, { id, status }) {
    const index = state.whatsappSendMessages.findIndex(
      (number) => number.id === id
    );

    state.whatsappSendMessages[index].state = status;
  },
};

export const actions = {
  async changeFilter({ dispatch, commit }, { key, value }) {
    commit("SET_FILTER", { key, value });

    dispatch("fetch");
  },

  async fetch({ commit, state }) {
    try {
      const { data } = await whatsappSendMessageApi.fetch(state.filter);

      commit("SET_WHATSAPP_NUMBER_HOSTS", data.data.data);
      commit("SET_TOTAL", data.data.total);

      return data.data.data;
    } catch (error) {
      console.log(error.response);
    }
  },
};
