export const menuItems = [
  {
    label: "Nomer Master",
    icon: "ri-shield-user-line",
    link: "/whatsapp-number-host",
    roles: ["ADMIN"],
  },
  {
    label: "Histori Pesan",
    icon: "ri-shield-user-line",
    link: "/whatsapp-history-messages",
    roles: ["ADMIN"],
  },
  {
    label: "Blaster",
    icon: "ri-shield-user-line",
    link: "/whatsapp-blaster",
    roles: ["ADMIN"],
  },
  {
    label: "Master Admin",
    icon: "ri-shield-user-line",
    link: "/admin",
    roles: ["ADMIN"],
  },

  {
    label: "Pengaturan",
    icon: "ri-gear-line",
    link: "/setting",
    roles: ["ADMIN"],
  },
];
