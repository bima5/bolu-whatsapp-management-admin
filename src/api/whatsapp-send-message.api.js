import api from "./index";

const baseURL = "/whatsapps";

const fetch = (params) => api.get(`${baseURL}`, { params });

export default {
  fetch,
};
