import api from "./index";

const baseURL = "/authentications";

const login = (payload) => api.post(`${baseURL}/login`, payload);

export default { login };
