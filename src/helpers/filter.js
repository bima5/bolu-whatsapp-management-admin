const filterDefault = {
  search: "",
  limit: 25,
  page: 1,
  ordering: "-created_at",
};

export default filterDefault;
