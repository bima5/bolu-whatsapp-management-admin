import store from "@/state/store";

export default [
  {
    path: "/login",
    name: "login",
    component: () => import("../views/pages/account/login"),
    meta: {
      beforeResolve(routeTo, routeFrom, next) {
        if (store.getters["auth/loggedIn"]) {
          next({ name: "home" });
        } else {
          next();
        }
      },
    },
  },
  {
    path: "/register",
    name: "register",
    component: () => import("../views/pages/account/register"),
    meta: {
      beforeResolve(routeTo, routeFrom, next) {
        // If the user is already logged in
        if (store.getters["auth/loggedIn"]) {
          // Redirect to the home page instead
          next({ name: "home" });
        } else {
          // Continue to the login page
          next();
        }
      },
    },
  },
  {
    path: "/forgot-password",
    name: "Forgot-password",
    component: () => import("../views/pages/account/forgot-password"),
    meta: {
      beforeResolve(routeTo, routeFrom, next) {
        // If the user is already logged in
        if (store.getters["auth/loggedIn"]) {
          // Redirect to the home page instead
          next({ name: "home" });
        } else {
          // Continue to the login page
          next();
        }
      },
    },
  },
  {
    path: "/logout",
    name: "logout",
    meta: {
      authRequired: true,
      beforeResolve(routeTo, routeFrom, next) {
        if (process.env.VUE_APP_DEFAULT_AUTH === "firebase") {
          store.dispatch("auth/logOut");
        } else {
          store.dispatch("authfack/logout");
        }
        const authRequiredOnPreviousRoute = routeFrom.matched.some((route) =>
          route.push("/login")
        );
        // Navigate back to previous page, or home as a fallback
        next(authRequiredOnPreviousRoute ? { name: "home" } : { ...routeFrom });
      },
    },
  },
  {
    path: "/",
    name: "home",
    meta: {
      authRequired: true,
    },
    component: () => import("../views/pages/dashboard/index"),
  },

  // Start Member Route
  {
    path: "/member",
    name: "list-member",
    meta: {
      authRequired: true,
    },
    component: () => import("../views/pages/member/index"),
  },
  {
    path: "/member/create",
    name: "create-member",
    meta: {
      authRequired: true,
    },
    component: () => import("../views/pages/member/form"),
  },
  {
    path: "/member/:id/edit",
    name: "edit-member",
    meta: {
      authRequired: true,
    },
    component: () => import("../views/pages/member/form"),
  },
  {
    path: "/member/:id/detail",
    name: "detail-member",
    meta: {
      authRequired: true,
    },
    component: () => import("../views/pages/member/detail"),
  },
  // Finish Member Route

  // Start Admin Route
  {
    path: "/admin",
    name: "list-admin",
    meta: {
      authRequired: true,
    },
    component: () => import("../views/pages/admin/index"),
  },
  {
    path: "/admin/create",
    name: "create-admin",
    meta: {
      authRequired: true,
    },
    component: () => import("../views/pages/admin/form"),
  },
  {
    path: "/admin/:id/edit",
    name: "edit-admin",
    meta: {
      authRequired: true,
    },
    component: () => import("../views/pages/admin/form"),
  },
  // Finish Admin Route

  // Start Whatsapp Number Host Route
  {
    path: "/whatsapp-number-host",
    name: "list-whatsapp-number-host",
    meta: {
      authRequired: true,
    },
    component: () => import("../views/pages/whatsapp-number-host/index"),
  },
  {
    path: "/whatsapp-number-host/create",
    name: "create-whatsapp-number-host",
    meta: {
      authRequired: true,
    },
    component: () => import("../views/pages/whatsapp-number-host/form"),
  },
  {
    path: "/whatsapp-number-host/:id/edit",
    name: "edit-whatsapp-number-host",
    meta: {
      authRequired: true,
    },
    component: () => import("../views/pages/whatsapp-number-host/form"),
  },
  // Finish Whatsapp Number Host Route

  // Start Whatsapp Blaster Route
  {
    path: "/whatsapp-blaster",
    name: "list-whatsapp-blaster",
    meta: {
      authRequired: true,
    },
    component: () => import("../views/pages/whatsapp-blaster/index"),
  },
  {
    path: "/whatsapp-blaster/:id/queue",
    name: "queue-whatsapp-blaster",
    meta: {
      authRequired: true,
    },
    component: () => import("../views/pages/whatsapp-blaster/detail"),
  },
  {
    path: "/whatsapp-blaster/create",
    name: "create-whatsapp-blaster",
    meta: {
      authRequired: true,
    },
    component: () => import("../views/pages/whatsapp-blaster/form"),
  },
  {
    path: "/whatsapp-blaster/:id/edit",
    name: "edit-whatsapp-blaster",
    meta: {
      authRequired: true,
    },
    component: () => import("../views/pages/whatsapp-blaster/form"),
  },

  {
    path: "/whatsapp-history-messages",
    name: "edit-whatsapp-history-messages",
    meta: {
      authRequired: true,
    },
    component: () => import("../views/pages/whatsapp-send-message/index"),
  },

  // Start Setting Route
  {
    path: "/setting",
    name: "setting",
    meta: {
      authRequired: true,
    },
    component: () => import("../views/pages/setting/index"),
  },
  // Finish Setting Route
];
