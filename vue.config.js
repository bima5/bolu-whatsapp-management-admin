module.exports = {
  publicPath: process.env.BUILD_PATH,
  pwa: {
    name: "Admin Kelas Bolu",
    themeColor: "#303237",
    workboxOptions: {
      exclude: [/OneSignal.*\.js$/, /^_/],
    },
  },
  chainWebpack: (config) => config.resolve.symlinks(false),
};
